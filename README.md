# playground

101camp1py ~ 蟒营 Python 入门班一期 游乐场仓库

## usage

在学习 git/gitlab 过程中,

- 如果有任何拿不准的操作,
- 先在 playground ~ 游乐场仓库 中尽情的嗯哼吧
- 不仅仅是 git , 还有其它各种 gitlab 功能都可以尝试/学习/理解/创造/...

## alert

- 给大家的权限是最高级别的
- 所以, 千万别轻易删除重大的东西, 以免无法恢复
- 以及:
    + 这只是 游乐场
    + 所有正式的作业/记录/交流
    + 都应该在: https://gitlab.com/101camp/1py/tasks


